from task_queues.queue import ConsumerEmitterTaskRunner as CETR
from task_queues.queue import ConsumerTaskRunner as CTR
import pymongo
from bson.objectid import ObjectId

class MongoTaskRunner(CETR):
    def _set_up(self):
        self._start_mongo()

    def _start_mongo(self):
        config = self._config['mongo']
        mongo_client = pymongo.MongoClient(config['host'], config['port'])
        self._db = mongo_client[config['db']]

    def update_by_id(self, _id, data):
        oid = ObjectId(_id)
        collection = self._config['mongo']['collection']
        result = self._db[collection].update_one({'_id':oid},{'$set': data})
        return True

class ConsumerMongoTaskRunner(CTR):
    def _set_up(self):
        self._start_mongo()

    def _start_mongo(self):
        config = self._config['mongo']
        mongo_client = pymongo.MongoClient(config['host'], config['port'])
        self._db = mongo_client[config['db']]

    def update_by_id(self, _id, data):
        oid = ObjectId(_id)
        collection = self._config['mongo']['collection']
        result = self._db[collection].update_one({'_id':oid},{'$set': data})
        return True
