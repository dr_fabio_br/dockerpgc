#! /usr/bin/env python3

# Path hack.
import sys

from sys import path
from ImageConsumer import ConsumerMongoTaskRunner as CMTR
from task_queues.queue import run_task_runner

import os.path as os_path
base_path = os_path.dirname(os_path.realpath(__file__))
root_path = os_path.dirname(base_path)
path.insert(0, root_path)

import numpy as np
import cv2
import random
import argparse
import boxDetector.checkBoxDetector as cbd


class BoxDetector(CMTR):
    def __init__(self, upload_dir, config):
        self._upload = upload_dir
        super().__init__(config)

    def task(self, key, data):
        fileName = self._upload+'raw/'+data['name']
        filled = cbd.get_filled_contours(fileName)
        update_data = {'checkboxes':filled}
        _id = data['_id']
        self.update_by_id(_id, update_data)

if __name__ == "__main__":
    upload_dir = sys.argv[1] if len(sys.argv) > 1 else "%s/tmp/" % root_path
    cfg =  {
        "host":"mq",
        "exchange":"file_exchange",
        "topic":"file.raw.#",
        "mongo":{
            "host":"mongo",
            "db":"tanDocs",
            "collection":"docs",
            "port":27017
        }
    }
    consumer = BoxDetector(upload_dir, cfg)
    run_task_runner(consumer)
