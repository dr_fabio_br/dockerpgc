import sys

from task_queues.queue import QEmitter

host = 'mq'
exchange = 'img_transform'
m = QEmitter(host,exchange)

routing_key = sys.argv[1] 
img = sys.argv[2]
body = {'img':img}
m.send(routing_key,body)
m.close()
print(" [x] Sent %r:%r" % (routing_key,body))

