#!/usr/bin/env python3
from task_queues.queue import run_task_runner, ConsumerTaskRunner as CTR
from sys import path
import sys
from bson.objectid import ObjectId
from focUtils.collections import default_named_tuple as def_named_tuple
from elasticsearch import Elasticsearch
import logging
import codecs

class OCR_Consumer(CTR):
    def __init__(self, upload_dir, config):
        self._upload = upload_dir
        self._log = logging.getLogger(__name__)
        super().__init__(config)

    def task(self, key, data):
        self._log.info("Received key %s ", key)

    def _set_up(self):
        self._start_elastic()

    def _start_elastic(self):
        config = self._config['elasticsearch']
        self._es = Elasticsearch(config['host'])

    def task(self, key, data):
       return self._save_ocr_content(data['_id'],data['ocr'])

    def _save_ocr_content(self, _id, ocr_file):
        config = self._config['elasticsearch']
        index = config['index'] #DB
        doc = config['doc_type']  #Table
        to_open = self._upload +'ocr/'+ ocr_file
        with codecs.open(to_open, 'r', "utf-8") as f:
            txt = f.read()
        body = {
            'content':txt
        }
        res = self._es.index(index=index, doc_type= doc, id= _id, body=body)
        return res['created']

if __name__ == "__main__":
    upload_dir = sys.argv[1] if len(sys.argv) > 1 else "%s/tmp/" % root_path
    logging.basicConfig(level=logging.INFO)

    cfg =  {
        "host":"mq",
        "exchange":"file_exchange",
        "topic":"img.ocr.#",
        "mongo":{
            "host":"mongo",
            "db":"tanDocs",
            "collection":"docs",
            "port":27017
        },
        "elasticsearch":{
            "host":"elasticsearch",
            "index":"tan_docs",
            "doc_type":"docs"
        }
    }
    consumer = OCR_Consumer(upload_dir, cfg)
    run_task_runner(consumer)
