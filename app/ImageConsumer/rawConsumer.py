#!/usr/bin/env python3
from subprocess import call
from focUtils.nameGenerator import getRandomName
from task_queues.queue import run_task_runner
from ImageConsumer import MongoTaskRunner as MTR
from sys import path
from collections import namedtuple
import logging

import sys

import os.path as os_path
base_path = os_path.dirname(os_path.realpath(__file__))
root_path = os_path.dirname(base_path)

TiffRet = namedtuple("TiffRet", "success tiff")

class RawConsumer(MTR):

    def __init__(self, upload_dir, config):
        self._upload = upload_dir
        self._log = logging.getLogger(__name__)
        super().__init__(config)

    def task(self, key, data):
        print("RECEBI!")
        self._log.info("Received key %s ", key)
        self._log.info("Received data %s ", data)
        
        res = self.make_tiff(data["name"])
        self._log.debug("Saved tiff %r ", res.success)
        if res.success:
            db_success = self.save_tiff_data(data["_id"], res.tiff)
            return TiffRet(db_success, res.tiff)
        return res

    def emitter_task(self, key, data, result):
        if result.success:
            self._log.debug("Tiff name %s ", result.tiff)
            routing_key = "img.treated"
            _id = data["_id"]
            tiff = result.tiff
            body = {"img": tiff, "_id": _id}
            self._emitter.send(routing_key, body)
        return result

    def save_tiff_data(self,_id, tiff):
        data = {"tiff": tiff}
        return self.update_by_id(_id, data)

    def make_tiff(self, origin):
        tiffName = getRandomName()+".tiff"
        upload_dir = self._upload
        params = [
            "convert",
            "-density", "300",
            "-depth", "8",
            "-background", "white",
            "-alpha", "remove",
            "-black-threshold", "86%",
            upload_dir+'raw/'+origin,
            upload_dir+'tiff/'+tiffName
        ]
        retcode = call(params)
        if retcode < 0:
            return Ret(False, None)
        return TiffRet(True, tiffName)

if __name__ == "__main__":
    upload_dir = sys.argv[1] if len(sys.argv) > 1 else "%s/tmp/" % root_path
    logging.basicConfig(level=logging.INFO)

    cfg =  {
        "host":"mq",
        "exchange":"file_exchange",
        "topic":"file.raw.#",
        "mongo":{
            "host":"mongo",
            "db":"tanDocs",
            "collection":"docs",
            "port":27017
        }
    }
    consumer = RawConsumer(upload_dir, cfg)
    run_task_runner(consumer)
