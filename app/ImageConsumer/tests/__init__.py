from unittest.mock import Mock, MagicMock, patch

config = {
    'host':'foo',
    'exchange':'bar',
    'topic':'baz.zip.toc',
    'mongo':{
        'host':'mhost',
        'port':'mport',
        'db':'db',
        'collection':'docs'
    }
}

def get_mongo_mock():
    mock_mongo = MagicMock()
    my_dict = {'docs': MagicMock(),'db':MagicMock()}
    def getitem(name):
        return my_dict[name]
    
    mock_mongo.__getitem__.side_effect = getitem
    my_dict['db'].__getitem__.side_effect = getitem
    return mock_mongo

def set_consumer_mocks(consumer,config):
    consumer._emitter = Mock()
    #Setting config since we are mocking parent
    consumer._config = config

def check_mongo_init(mock_mongo, mock_client):
    mock_client.assert_called_with(config['mongo']['host'], config['mongo']['port'])
    db_name = config['mongo']['db']
    mock_mongo.__getitem__.assert_called_with(db_name)

def patch_consumers(module_name):
    """ Decorator to patch methods commonly used on CETRs
    """
    def func_wrapper(func):
        @patch('ImageConsumer.ObjectId') 
        @patch('ImageConsumer.pymongo.MongoClient') 
        @patch(module_name+'.call') 
        @patch(module_name+'.MTR')
        def wrapper(*args,**kwargs):
            return func(*args,**kwargs)
        return wrapper
    return func_wrapper