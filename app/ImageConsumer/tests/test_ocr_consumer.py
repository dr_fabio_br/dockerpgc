import unittest

from unittest.mock import Mock, patch, MagicMock, mock_open
from ImageConsumer.ocrConsumer import OCR_Consumer
from focUtils.tests import spy

config = {
    'host':'foo',
    'exchange':'bar',
    'topic':'baz.zip.toc',
    'elasticsearch':{
        'index':'fake_index',
        'doc_type':'fake_doc',
        'host':'fake_host'
    }
}
class OCR_ConsumerTest(unittest.TestCase):
    @patch('ImageConsumer.ocrConsumer.Elasticsearch') 
    def test_consumer(self, mock_es_creator):
        up_dir = '/tmp/'
        mock_es = Mock()
        mock_es_creator.return_value = mock_es
        consumer = OCR_Consumer(up_dir,config)
        consumer._config = config
        consumer._set_up()
        #Checking elastic search init
        mock_es_creator.assert_called_with(config['elasticsearch']['host'])
        key = 'foo.bar.baz'
        data = {'ocr':'foo','_id':'_id'}
        m_open = mock_open(read_data='Foo contents')
        mock_es.index = MagicMock()
        mock_es.index.return_value = {'created':True}
        with patch('ImageConsumer.ocrConsumer.open', m_open, create=True):
            consumer.task(key,data)
