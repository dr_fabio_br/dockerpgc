import unittest

from unittest.mock import Mock, patch, MagicMock
from ImageConsumer.rawConsumer import RawConsumer
from focUtils.tests import spy
from ImageConsumer.tests import (
    set_consumer_mocks,
    get_mongo_mock, 
    config, 
    check_mongo_init,
    patch_consumers
)

class RawConsumerTest(unittest.TestCase):
    @patch_consumers("ImageConsumer.rawConsumer")
    def test_consumer(self, m_CETR, m_call, m_client, m_id):
        up_dir = '/tmp/'
        mock_mongo = get_mongo_mock()
        m_client.return_value= mock_mongo
        m_id.return_value = 'oid'

        consumer = RawConsumer(up_dir,config)
        set_consumer_mocks(consumer,config)
        #Mocking what start up would do
        consumer._set_up()
        #Check mongo setup
        check_mongo_init(mock_mongo, m_client)
        #Testing the task
        data = {'name':'name','_id':'_id'}
        key = 'foo.bar.baz'
        m_call.return_value = 0
        #Spying
        with spy(consumer, 'make_tiff') as make_tiff:
            with spy(consumer, 'save_tiff_data') as save_tiff:
                task_ret = consumer.task(key, data)
                make_tiff.assert_called_with(data['name'])
                self.assertTrue(m_call.called)
                #Check it saved on the db
                save_tiff.assert_called_with(data['_id'], task_ret.tiff)
                update_data = {'$set':{'tiff':task_ret.tiff}}
                where = {'_id': m_id.return_value }
                mock_mongo['docs'].update_one.assert_called_with(where, update_data)

        
        consumer.emitter_task(key, data, task_ret)
        body = {'img':task_ret.tiff,'_id':data['_id']}
        consumer._emitter.send.assert_called_with('img.treated',body)