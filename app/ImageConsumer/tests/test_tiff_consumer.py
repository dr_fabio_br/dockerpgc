import unittest

from unittest.mock import Mock, patch, MagicMock
from ImageConsumer.tiffConsumer import TiffConsumer
from focUtils.tests import spy
from ImageConsumer.tests import (
    set_consumer_mocks,
    get_mongo_mock, 
    config, 
    check_mongo_init,
    patch_consumers
)
class TiffConsumerTest(unittest.TestCase):
    ## TODDO test other use cases
    @patch_consumers("ImageConsumer.tiffConsumer")
    def test_consumer(self, m_CETR, m_call, m_client, m_id):
        up_dir = '/tmp/'
        mock_mongo = get_mongo_mock()
        m_client.return_value= mock_mongo
        m_id.return_value = 'oid'
        m_call.return_value = 0

        consumer = TiffConsumer(up_dir,config)
        set_consumer_mocks(consumer,config)
        #Mocking what start up would do
        consumer._set_up()
        check_mongo_init(mock_mongo, m_client)
        data = {'img':'tiff','_id':'_id'}
        key = 'img.treated'
        with spy(consumer, 'make_ocr') as make_ocr:
            with spy(consumer, 'make_hocr') as make_hocr:            
                with spy(consumer, 'save_on_db') as save_on_db:
                    task_ret = consumer.task(key, data)
                    #Checking ocr
                    calls = m_call.call_args_list
                    ocr_call = calls[0]
                    make_ocr.assert_called_with(data['img'])
                    ## No cwd
                    self.assertEqual(ocr_call[1],{})
                    #Checking hocr
                    make_hocr.assert_called_with(data['img'])
                    hocr_call = calls[1]
                    # yes cwd
                    self.assertEqual(list(hocr_call[1].keys())[0], 'cwd')
                    # Testing save on dv
                    update_call = mock_mongo['docs'].update_one.call_args_list[0]
                    update_call = update_call[0]
                    self.assertEqual(update_call[0],{'_id':m_id.return_value})
                    mongo_set = update_call[1]['$set']
                    ## TODO test the returns values on the functions
                    ocr = task_ret.ocr_result.ocr
                    hocr = task_ret.hocr_result.hocr
                    self.assertEqual(mongo_set['ocr'], ocr)
                    self.assertEqual(mongo_set['hocr'], hocr)
                    
        consumer.emitter_task(key, data, task_ret)
        body = {'ocr': ocr,'hocr': hocr, '_id':data['_id']}
        consumer._emitter.send.assert_called_with('img.ocr',body)