#!/usr/bin/env python3
from subprocess import call
from focUtils.nameGenerator import getRandomName
from task_queues.queue import run_task_runner
from ImageConsumer import MongoTaskRunner as MTR
from sys import path
from focUtils.collections import default_named_tuple as def_named_tuple

import logging
import os
import sys

import os.path as os_path
base_path = os_path.dirname(os_path.realpath(__file__))
root_path = os_path.dirname(base_path)

TaskRet = def_named_tuple("TaskRet", "success ocr_result hocr_result db_result")
OCR_Ret = def_named_tuple("OCR_Ret", "success ocr")
HOCR_Ret = def_named_tuple("HOCR_Ret", "success hocr")

baseDir = os.path.dirname(__file__)

class TiffConsumer(MTR):
    def __init__(self, upload_dir, config):
        self._upload = upload_dir
        self._ocrDir = self._upload+"ocr/"
        self._tiffDir = self._upload+"tiff/"
        self._log = logging.getLogger(__name__)
        super().__init__(config)
  
    def task(self, key, data):
        ##TODO paralize or separate tasks
        self._log.info("Received key %s ", key)
        tiff_name = self._tiffDir+data['img']
        
        r_ocr = self.make_ocr(tiff_name)
        if not r_ocr.success:
            return TaskRet( False, r_ocr)
        self._log.info("Successfully made OCR")
        r_hocr = self.make_hocr(tiff_name)
        if not r_hocr.success:
            return TaskRet( False, r_ocr, r_hocr)
        self._log.info("Successfully made HOCR")

        _id = data['_id']
        r_db = self.save_on_db(r_ocr.ocr, r_hocr.hocr, _id)
        if not r_db:
            return  TaskRet(False,r_ocr, h_ocr, r_db)
        self._log.info("Successfully Saved on db")

        return  TaskRet(True,r_ocr, r_hocr, r_db)

    def emitter_task(self, key, data, result):
        if result.success:
            _id = data['_id']
            ocr = result.ocr_result.ocr
            hocr = result.hocr_result.hocr
            
            body = {'ocr': ocr,'hocr': hocr, '_id': _id}
            routing_key = "img.ocr"
            self._emitter.send(routing_key, body)
        return result

    def make_ocr(self,tiff):
        ocrFileName = getRandomName()
        retcode = call(["tesseract",tiff,self._ocrDir+ocrFileName,"-l","por"])
        if retcode < 0:
            return OCR_Ret(False)
        return OCR_Ret(True, ocrFileName+'.txt')
    
    def make_hocr(self,tiff):
        hocrFileName = getRandomName()
        retcode = call(["tesseract",tiff,self._ocrDir+hocrFileName,"-l","por","+configData/hocr.txt"],cwd=baseDir+"/cmd")
        if retcode < 0:
            return HOCR_Ret(False)
        return HOCR_Ret(True, hocrFileName+'.hocr')

    def save_on_db(self, ocr, hocr, _id):
        data = {'ocr': ocr, 'hocr': hocr}
        return self.update_by_id(_id,data)

if __name__ == '__main__':
    upload_dir = sys.argv[1] if len(sys.argv) > 1 else "%s/tmp/" % root_path
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)
    cfg =  {
        "host":"mq", 
        "exchange":"file_exchange",
        "topic":"img.treated.#",
        "mongo":{
            "host":"mongo",
            "db":"tanDocs",
            "collection":"docs",
            "port":27017
        }
    }
    consumer = TiffConsumer(upload_dir, cfg)
    run_task_runner(consumer)
