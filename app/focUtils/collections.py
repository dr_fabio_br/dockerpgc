from collections import namedtuple

def default_named_tuple(*args, **kwargs):
    named_tuple = namedtuple(*args, **kwargs)
    named_tuple.__new__.__defaults__ = (None,) * len(named_tuple._fields)
    return named_tuple
