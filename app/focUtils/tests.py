from unittest.mock import patch

def spy(target, attribute, **kwargs):
    if kwargs.get('wraps') is None:
        kwargs['wraps'] = getattr(target, attribute)
    return patch.object(target, attribute , **kwargs)