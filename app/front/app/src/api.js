import config from './config';
let rpc_id = 1;

export const jsonRPC = (url, method, data, baseUrl = config.apiURL) => {
    return new Promise((resolve,reject)=>{
        const fullUrl = baseUrl + url;
        let rpc_data = {
            jsonrpc:"2.0",
            method:method,
            params:data,
            id: ++rpc_id
        }
        const errorCb = (xhr,status,errText) =>{
            reject({
                    code:-32000,
                    message:' Http error',
                    data:{
                        status:status,
                        text:errText
                    }
            })
        }
        const successCb = (data,status,xhr) => {
            if(data.error){
                reject(data.error)
                return;
            }
            if(data.result){
                resolve(data.result)
            }
        }
        rpc_data = JSON.stringify (rpc_data)
        $.ajax({
            method:'POST',
            url: fullUrl,
            data: rpc_data,
            success: successCb,
            contentType: "application/json",
            dataType: 'json',
            'error': errorCb
        });      

    })
}
export default jsonRPC;