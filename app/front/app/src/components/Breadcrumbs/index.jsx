import React from 'react';

export const Breadcrumbs = React.createClass({
    render(){
        const size = this.props.items.length;
        let hasActive = false;
        return(
            <ol className="breadcrumb">
                {
                    this.props.items.map((item,index) =>{
                        if(typeof item !== 'object'){
                            item = {text:item};
                        }
                        let link = item.link || '#';
                        let className = item.active?'active':'';
                        if(className==='active'){
                            hasActive = true;
                        }
                        else if(index ===size-1){
                            className = 'active';
                        }
                        let text = item.text;
                        return (
                            <li  key={index}><a href={link} className={className}>{text}</a></li>
                        )
                    })

                }
            </ol>
        )
    }
})
