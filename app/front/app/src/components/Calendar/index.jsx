import Datetime from 'react-datetime'
import React from 'react';

require('../../../../node_modules/react-datetime/css/react-datetime.css')
export default React.createClass({
    contextTypes: {
        errorMap: React.PropTypes.object
    },
    handleChange(value) {
        const field = this.props.field
        this.props.onChange(field, value)
    },
    help(){
        const field = this.props.field
        if(typeof this.context.errorMap=='undefined') return
        if(this.context.errorMap.hasOwnProperty(field)){
            return this.context.errorMap[field][0]
        }
    },
    validationState(){
        const field = this.props.field
        if(typeof this.context.errorMap=='undefined') return
        if(this.context.errorMap.hasOwnProperty(field)){
            return 'error'
        }
    },
    render(){
        let containerClass = 'form-group has-feedback'
        let help = null
        if(this.validationState() === 'error'){
            containerClass+=' has-error'
            help = (
                <span className="help-block">{this.help()}</span>
            )
        }
        return(
            <div className={containerClass}>
                <label className="control-label">{this.props.label}</label>
                    <Datetime 
                        dateFormat="DD/MM/YYYY" 
                        timeFormat={false} 
                        value = {this.props.value}
                        className="input-wrapper"
                        inputProps={{placeholder:this.props.label,className:'form-control'}}
                        onChange={this.handleChange} 
                        closeOnSelect={true} />
                {help}
            </div>
        )
    }
})
