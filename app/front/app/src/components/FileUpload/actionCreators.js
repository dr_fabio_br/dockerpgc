handleChange(e){
    e.preventDefault();
    let files = [];
    for (let i = 0; i < e.target.files.length; i++) {
        // Convert to Array.
        files.push(e.target.files[i]);
    }
    Promise.all(files.map(file => new Promise((resolve, reject) => {
        let reader = new FileReader();

        reader.onload = read => {
            // Resolve both the FileReader result and its original file.
            resolve({read, original: file});
        };

        // Read the file with format based on this.props.as.
        switch ((this.props.as).toLowerCase()) {
            case 'binary': {
              reader.readAsBinaryString(file);
              break;
            }
            case 'buffer': {
              reader.readAsArrayBuffer(file);
              break;
            }
            case 'text': {
              reader.readAsText(file);
              break;
            }
            case 'url': {
              reader.readAsDataURL(file);
              break;
            }
        }
    })))
    .then(zippedResults => {
        // Run the callback after all files have been read.
        this.props.onChange(e, zippedResults);
    });
}  