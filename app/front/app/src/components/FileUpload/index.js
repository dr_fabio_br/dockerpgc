import React from 'react';
import {connect} from 'react-redux';
const FileUploadDisplay = React.createClass({
    render(){
        if (!window.File || !window.FileReader || !window.FileList || !window.Blob) {
            console.warn(
                '[react-file-reader-input] Some file APIs detected as not supported.' +
                ' File reader functionality may not fully work.'
            );
        }
        return(
            <div className="form-inline fileUpload">
                <div className="form-group">
                    <label htmlFor={this.props.name}>{this.props.label}</label><input onChange={this.props.handleChange} className="form-control fileUpload__input" name={this.props.name} type="file"/>
                </div>
            </div>
        )
    }
});
const actions = {
    handleChange(e,openAs = 'binary'){
        e.preventDefault();
        return (dispatch,getState) => {
            const state = getState();
            let files = [];
            for (let i = 0; i < e.target.files.length; i++) {
                // Convert to Array.
                files.push(e.target.files[i]);
            }
            Promise.all(files.map(file => new Promise((resolve, reject) => {
                let reader = new FileReader();
                reader.onload = read => {
                    // Resolve both the FileReader result and its original file.
                    resolve({read, original: file});
                };

                // Read the file with format based on this.props.as.
                switch (openAs.toLowerCase()) {
                    case 'binary': {
                      reader.readAsBinaryString(file);
                      break;
                    }
                    case 'buffer': {
                      reader.readAsArrayBuffer(file);
                      break;
                    }
                    case 'text': {
                      reader.readAsText(file);
                      break;
                    }
                    case 'url': {
                      reader.readAsDataURL(file);
                      break;
                    }
                }
            })))
            .then(zippedResults => {
                dispatch({'type': 'FILE_CHANGE','event': e, 'results':zippedResults});
            });
            
        }
    } 
};
const mapDispatchToProps = function(dispatch,ownProps){
    return {
        handleChange: function(e){ dispatch(actions.handleChange(e,ownProps.as)); },
    }
};

export const FileUpload = connect(null,mapDispatchToProps)(FileUploadDisplay);
require('./fileUpload.scss');