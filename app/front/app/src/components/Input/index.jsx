import React from 'react';
import {Input} from 'react-bootstrap'
export default React.createClass({
    
    handleChange() {
        const field = this.props.field
        const value = this.refs[field].getValue()
        this.props.onChange(field, value)
    },
    help(){
        const field = this.props.field
        if(typeof this.context.errorMap=='undefined') return
        if(this.context.errorMap.hasOwnProperty(field)){
            return this.context.errorMap[field][0]
        }
    },
    validationState(){
        const field = this.props.field
        if(typeof this.context.errorMap=='undefined') return
        if(this.context.errorMap.hasOwnProperty(field)){
            return 'error'
        }
    },
    render() {
        const {type,placeholder,label,field, ...other} = this.props
        return (
             <Input
                    {...other}
                    type={type || 'text'}
                    placeholder={this.props.placeholder || this.props.label}
                    label={label}
                    hasFeedback
                    ref={this.props.field}
                    help={this.help()}
                    bsStyle={this.validationState()}
                    onChange={()=>{this.handleChange()}}  />
        );
    },
    contextTypes: {
        errorMap: React.PropTypes.object
    }
  
});
