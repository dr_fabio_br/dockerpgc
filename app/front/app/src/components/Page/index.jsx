import React from 'react';

export const Page = React.createClass({
    render(){
        
        return(
            <div className="page container">
                <div className="page-header">
                    <h1 className="page__title">{this.props.title}</h1>
                </div>
                {this.props.children}
            </div>
        )
    }
})
require('./page.scss');