import React from 'react';

export const Panel = React.createClass({
    render(){
    
        return(
            <div className="panel panel-primary">
                {this.props.children}
            </div>
        )
    }
});
Panel.header = React.createClass({
    render(){
        
        return(
            <div className="panel-heading">
                <h3 className="page__title">{this.props.title}</h3><div className="panelContainer">{this.props.children}</div>
            </div> 
        )
    }
});
Panel.body = React.createClass({
    render(){
        
        return(
            <div className="panel-body"> 
                {this.props.children}
            </div> 
        )
    }
});
require('./panel.scss');