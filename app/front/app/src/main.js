// require('./navbar/index.js');

import React from 'react';
import ReactDOM from 'react-dom';
import { Home , Docs } from './pages';
import reducer from './reducers/tanDocs'
import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';
import { Provider } from 'react-redux'
import { Router, Route, browserHistory, IndexRoute,IndexLink, hashHistory, Link } from 'react-router'
import { syncHistory, routeReducer } from 'react-router-redux'
import {Navbar,NavItem,Nav,NavDropdown,MenuItem} from 'react-bootstrap'
const App = React.createClass({
    render: function(){
        return (
            <div>
                <Navbar inverse>
                    <Navbar.Header>
                        <Navbar.Brand>
                            <a href="/">TanDocs</a>
                        </Navbar.Brand>
                    <Navbar.Toggle />
                    </Navbar.Header>
                    <Navbar.Collapse>
                    <Nav>
                        <NavItem eventKey={1} href="/docs">Documentos</NavItem>
                    </Nav>
                    </Navbar.Collapse>
                </Navbar>
                {this.props.children}
            </div>
        )
    }
});
const router = <Router history={browserHistory}>
    <Route path="/" component={App}>
        <IndexRoute component={Home}/>
        <Route path="/docs" component={Docs}/>
    </Route>
</Router>;
const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
const store = createStoreWithMiddleware(reducer);

const app = <Provider store = {store}>{router}</Provider>;
ReactDOM.render(app, document.getElementById('app'));
require('./main.scss');