"use strict"
import jsonRPC from '../../api'
const SEARCH_RESPONSE = 'DOCS_SEARCH_RESPONSE'
const SEARCH_FAILUTE_RESPONSE = 'DOCS_SEARCH_FAILURE_RESPONSE'
const SEARCHING = 'DOCS_SEARCHING'
const VIEWING = 'DOCS_VIEWING'
const VIEW_RESPONSE = 'DOCS_VIEW_RESPONSE'
const VIEW_FAILURE = 'DOCS_VIEW_FAILURE'
const CLOSE_VIEW = 'DOCS_CLOSE_VIEW'

export const types ={
    SEARCH_RESPONSE,
    SEARCH_FAILUTE_RESPONSE,
    SEARCHING,
    VIEW_RESPONSE,
    VIEW_FAILURE,
    CLOSE_VIEW,
    VIEWING
}
export const searchDocs = (searchString) =>{
    const data = {searchString}
    return (dispatch,getState) => {
        const type = SEARCHING
        dispatch({type})
        jsonRPC('docs','search',data).then(
            (data) =>{
                const type = SEARCH_RESPONSE
                const action = {type,searchResult:data}
                dispatch(action)
            },
            (error)=>{
                const type = SEARCH_FAILUTE_RESPONSE

                const action = {type,failureReponse:error}
                dispatch(action)
            }
        )
    }
}
export const viewDoc = (item) =>{
    return (dispatch,getState) => {
        console.log(types.VIEWING);
        dispatch({type:types.VIEWING,item})
        jsonRPC('docs','readText',{file:item.ocr}).then(
            (itemText) =>{
                const type = VIEW_RESPONSE
                const action = {type,itemDisplay:item.name,itemText}
                dispatch(action)
            },
            (error)=>{
                const type = VIEW_FAILURE

                const action = {type,failureReponse:error}
                dispatch(action)
            }
        )
    }
}