import {Modal,Button, Alert,ButtonGroup } from 'react-bootstrap'
import React from 'react';
import config from '../../../config'

export default  React.createClass({
     getInitialState() {
        return {display:'img'}
    },
    showImg(){
        this.setState({display:'img'})
    },
    showText(){
        this.setState({display:'text'})
    },
    render(){
        let display = null
        if(this.state.display === 'text'){
            display = (
                <pre>
                    {this.props.itemText}
                </pre>
            )
        }
        else if(this.state.display === 'img'){
            display = (
                <img className="viewData--img" src={config.apiURL+'img/'+this.props.itemDisplay}/>
            )
        }
        return(
            <div >
                <ButtonGroup>
                    <Button onClick={this.showImg} bsStyle={this.state.display==='img'?'info':'default'}>Imagem</Button>
                    <Button onClick={this.showText} bsStyle={this.state.display==='text'?'info':'default'}>Texto</Button>
                </ButtonGroup>
                {display}  
            </div>
        )
    }
})
require('./viewData.scss')