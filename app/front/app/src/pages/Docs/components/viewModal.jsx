import {Modal,Button, Alert } from 'react-bootstrap'
import React from 'react';
export default  React.createClass({
    render(){
        return(
            <div className="static-modal">
                <Modal show={this.props.show} backdrop={true}>
                    <Modal.Header closeButton={true} onHide={this.props.onHide}>
                        <Modal.Title>Visualização</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {this.props.children}
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.props.onHide} disabled={this.props.processing}>Fechar</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
})