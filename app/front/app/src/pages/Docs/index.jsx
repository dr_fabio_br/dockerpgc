import React from 'react';
import { Page, Input} from '../../components';
import Table from 'react_sort_table'
import * as actions from './actions'
import {connect} from 'react-redux';
import moment from 'moment'
import {Button, Tooltip} from 'react-bootstrap'
import {types, viewDoc} from './actions'
import ViewModal from './components/viewModal'
import ViewData from './components/viewData'

export const DocsDisplay =  React.createClass({
    contextTypes :{
        store: React.PropTypes.object
    },
    componentDidMount() {
        this.context.store.dispatch(actions.searchDocs())
    },
    headers:{
        original_name:'Nome',
        date:'Data',
        highlight:'Resumo',
        actions:'Ações',
    },
    getInitialState: function() {
        return {searchString:''}
    },
    
    'dataFunctions':{
        date:(context, data)=>{
            if(!data.date){
                return ''
            }
            return moment(data.date.$date).format('DD/MM/YYYY')
        },
        actions:(context,data)=>{
            const toCall = ()=>{
                context.viewItem(data)
            }
            return (<a onClick={(e)=>{context.props.onViewItem(data)}}>Visualizar</a>)
           
        },
        highlight:(context,data)=>{
            if(typeof data.highlights == 'undefined' || data.highlights.length ==0){
                return ""
            }
            return data.highlights.map((highlight,index)=>{
                const start = highlight.indexOf('<em>')
                const end = highlight.indexOf('</em>')
                const left = highlight.substring(0,start)
                const middle =highlight.substring(start+4,end)
                const right =highlight.substring(end+5)
                return <p  key={index} >{left}<b>{middle}</b>{right}</p>
            })
            
        }
    },
     _handleSearchKeyPress(e){
        if (e.key === 'Enter') {
            this.context.store.dispatch(actions.searchDocs(this.state.searchString))
        }
    },
    searchChange(key,value){
        this.state.searchString = value
        this.setState(this.state) 
    },
    list:[{name:'a'}],
    render(){
        return (
            <Page title="Documentos">
                <ViewModal show={this.props.viewing} onHide={this.props.onCloseView}>
                    <ViewData itemText={this.props.itemText} itemDisplay={this.props.itemDisplay}/>
                </ViewModal>
                <div>
                    <Input type="text"  placeholder="Pesquisa..." field="textSearch" onChange={this.searchChange} onKeyPress = {this._handleSearchKeyPress} />
                </div>
                <Table  headers={this.headers}>
                     <Table.header/>
                      <Table.body
                        context = {this}
                        data={this.props.searchResult} 
                        dataFunctions={this.dataFunctions}
                        />
                </Table>
            </Page>
        )
    }
})

const mapStateToProps = (state,ownProps)=>{
    return state.docs.toJSON();
}

const mapDispatchToProps = (dispatch,ownProps)=>{
    return {
        onViewItem:(item)=>{
            dispatch(viewDoc(item))
        },
        onCloseView:()=>{
            const type = types.CLOSE_VIEW
            dispatch({type})
        }
    }
};
export const Docs = connect(mapStateToProps, mapDispatchToProps)(DocsDisplay);

