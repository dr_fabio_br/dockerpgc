import {fromJS} from 'immutable';
import {types} from './actions'
export const INITIAL_STATE = fromJS({searchResult:[]});

export default (state = INITIAL_STATE, action) => {
  if (!action.type.startsWith('DOCS_')){
    return state;
  }
  switch(action.type) {
    case types.SEARCH_RESPONSE:
      return state.withMutations((map)=>{
        return map.delete('searching')
          .set('searchResult',action.searchResult)
      })
    case types.SEARCH_FAILUTE_RESPONSE:
      return state.withMutations((map)=>{
        return map.delete('searching')
          .set('failureReponse',action.failureReponse)
      })
    case types.SEARCHING:
      return state.set('searching',true)
    case types.VIEWING:
      return state.withMutations((map)=>{
        return map.set('viewing',true)
          .set('itemViewed',action.item)
          .set('itemDisplay',action.item.name)
      })
    case types.VIEW_RESPONSE:
      return state.withMutations((map)=>{
        return map.set('viewing',true)
          .set('itemDisplay',action.itemDisplay)
          .set('itemText',action.itemText)
      })
    case types.VIEW_FAILURE:
      return state.withMutations((map)=>{
        return map.delete('viewing')
          .delete('itemViewed')
          .delete('itemDisplay')
      })
    case types.CLOSE_VIEW:
      return state.withMutations((map)=>{
        return map.delete('viewing')
          .delete('itemViewed')
          .delete('itemDisplay')
      })
  }

  return state;
}