import config from '../../config'
import jsonRPC from '../../api'

export const sendFiles = (files) =>{
    return (dispatch,getState) => {
        const docFile = files[0];
        const binaryString = docFile.read.target.result;
        const base64 = btoa(binaryString);
        const fileName = docFile.original.name;
        const data = {file: base64, file_name:fileName };
        jsonRPC('docs','create',data).then(
            (data) =>{
                dispatch({type: 'FILE_UPLOADED', result:data.result});            
            },
            (error)=>{
                dispatch({type: 'FILE_UPLOADED', error:error.data});            

            }
        )
    }
}