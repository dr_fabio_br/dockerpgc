
import React from 'react';
import {FileUpload, Page} from '../../components';
import {Alert} from 'react-bootstrap'
import {connect} from 'react-redux';
import * as actions from './actions'
export const HomeDisplay = React.createClass({

    render(){
        let conditional , alert;
        if(this.props.uploading){
            conditional = <Alert bsStyle="info"> <p><i className="fa fa-cog fa-spin"></i> Enviando </p></Alert>
        }
        if(this.props.errorMessage){
            alert =<Alert bsStyle="danger">{this.props.errorMessage}</Alert>
        }
         if(this.props.successMessage){
            alert = <Alert bsStyle="success">{this.props.successMessage}</Alert>
        }

        return (
            <Page title="tanDocs">
                {alert}
                <FileUpload label="Arquivo" name="docFile" />
                <button   onClick={this.sendFiles} disabled={!this.props.hasFiles && !this.props.uploading} >Enviar</button>
                {conditional}
            </Page>
        )
    },
    sendFiles:function(e){
        e.preventDefault();
        this.props.onUpload(this.props.files);
    }
});
const mapStateToProps = function(state,ownProps){
    const home = state.home;
    const files = home.get('files',null);
    const uploading = home.get('uploading',false);
    return {
        files, 
        hasFiles:!!files, 
        uploading:uploading,
        errorMessage: home.get('errorMessage'),
        successMessage: home.get('successMessage')
    };
}

const mapDispatchToProps = function(dispatch,ownProps){
    return {
        onUpload: function(files){ 
            dispatch({type:'FILE_UPLOADING'})
            dispatch(actions.sendFiles(files));
        }
    }
};
export const Home = connect(mapStateToProps, mapDispatchToProps)(HomeDisplay);

