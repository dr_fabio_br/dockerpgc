import {Map} from 'immutable';

export const INITIAL_STATE = Map();

export default (state = INITIAL_STATE, action) => {
  if (!action.type.startsWith('FILE_')){
    return state;
  }
  switch(action.type) {
      case 'FILE_CHANGE':
          return state.set('files',action.results);
      case 'FILE_UPLOADING':
        return state.withMutations(map => {
            map.set('uploading',true).delete('errorMessage').delete('successMessage');
        });
      case 'FILE_UPLOADED':
        const ret = state.withMutations(map => {
            map.set('uploading',false);
            if(action.error){
                map.set('errorMessage','Falha ao salvar, tente de novo')
            }
            else{
                map.set('successMessage','Arquivo salvo')
                .set('fileData',action.result);
            }
        });
        return ret;

  }
  return state;
}