import {Map} from 'immutable';
import { combineReducers } from 'redux';
import home from '../pages/Home/reducer';
import docs from '../pages/Docs/reducer';

export const INITIAL_STATE = Map();
const main = (state = INITIAL_STATE, action) => {
   console.log('AQUI');
   console.log(action);
   return state;
};

const reducer = combineReducers({ main, home, docs });
export default reducer;