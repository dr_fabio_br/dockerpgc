from sys import path
import os.path as os_path
base_path = os_path.dirname(os_path.realpath(__file__))
root_path = os_path.dirname(base_path)
path.insert(0, root_path)

import numpy as np
import cv2
import random
import argparse
import boxDetector.checkBoxDetector as cbd


parser = argparse.ArgumentParser(description='Process images.')
parser.add_argument("-i",dest='input_file')
parser.add_argument("-o",dest='output_file')

args = parser.parse_args()
def is_contour_interesting(contour):
    return True
# Load an color image
img = cv2.imread(args.input_file)
#Make it gray
imgray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
#reducing noise
ret,thresh = cv2.threshold(imgray,127,255,0)
#RETR_TREE make nested contours
#CHAIN_APPROX_SIMPLE    compresses horizontal, vertical, and diagonal segments and leaves only their end points.
_,contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

#hierarchy fix 
hierarchy = hierarchy[0]
#the first one is always the container
contours = contours[1:]

def print_colored_contours(contours):
    for i,cnt in enumerate(contours):
        color = (random.randint(0, 255),random.randint(0, 255),random.randint(0, 255))
        cv2.drawContours(img,[cnt],0,color,2)
    cv2.imwrite('out.png',img)

with open(args.output_file, 'w') as f:
    print("hierarchy")
    print(hierarchy)
    for i,cnt in enumerate(contours):
        i += 1
        #[0] = next contour at the same hierarchical level
        #[1] = previous contour at the same hierarchical level
        #[2] = denotes its first child contour
        #[3] = denotes index of its parent contour
        isClosedShape = hierarchy[i][2] != -1
        if isClosedShape:
            firtstChildHierarchy = hierarchy[hierarchy[i][2]]
            isFilled = firtstChildHierarchy[0]!=-1
            if isFilled:
                # cnt = cnt[0:8]
                for point in cnt:
                    f.write("%i,%i\n"%(point[0][0],point[0][1]))
                    print(point[0])
                color = (random.randint(0, 255),random.randint(0, 255),random.randint(0, 255))
                cv2.drawContours(img,[cnt],0,color,2)

            else:
                print("NOT CLOSED")

        else:
            print("NOT FILLED")
            continue
cv2.imwrite('out.png',img)