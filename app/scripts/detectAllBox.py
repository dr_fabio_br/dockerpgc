#! /usr/bin/env python3

# Path hack.
from sys import path
import os.path as os_path
base_path = os_path.dirname(os_path.realpath(__file__))
root_path = os_path.dirname(base_path)
path.insert(0, root_path)

import numpy as np
import cv2
import random
import argparse
import boxDetector.checkBoxDetector as cbd

img_path = root_path+'/imgs/checkbox.tiff'

def get_filled_contours(img_path,min_rect_size = 30):
    filled_coordinates = []
    img = cv2.imread(img_path)
    #Make it gray
    imgray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    #reducing noise
    ret,thresh = cv2.threshold(imgray,127,255,0)
    _,contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    #hierarchy fix and skipping outer container
    hierarchy = hierarchy[0][1:]
    #the first one is always the outer container skipping it 
    contours = contours[1:]
    for i,cnt in enumerate(contours):
            #[0] = next contour at the same hierarchical level
            #[1] = previous contour at the same hierarchical level
            #[2] = denotes its first child contour
            #[3] = denotes index of its parent contour
            child_index = hierarchy[i][2]
            isClosedShape = child_index != -1
            if isClosedShape:
                # -1 because we skipped the outer contour
                firtstChildHierarchy = hierarchy[child_index-1]
                isFilled = firtstChildHierarchy[0]!=-1
                if isFilled:
                    #Finds the minimun area rectangle for the contour
                    rect = cv2.minAreaRect(cnt)
                    box = cv2.boxPoints(rect)
                    width = box[3][0] - box[0][0]
                    if width< min_rect_size:
                        print("Not right size")
                        continue
                    contour_list=list(map(lambda x: x[0], cnt))
                    if not cbd.is_boxy(contour_list):
                        print("Is not boxy!")
                        continue
                        
                    filled_coordinates.append(cbd.Checkbox(box[0], box[1], box[2], box[3]))
                    print("IS BOXY")
                    box = np.int0(box)
                    color = (random.randint(0, 255),random.randint(0, 255),random.randint(0, 255))
                    cv2.drawContours(img,[cnt],0,color,2)
                    print("RECT")
                    print(rect)
                    print("BOX")
                    print(box)
                    print("ONE FILLED")
                else:
                    print("NOT Filled")
            else:
                continue
    cv2.imwrite('out.png',img)
    return filled_coordinates

filled = get_filled_contours(img_path)
print("FILLED")
print(filled)