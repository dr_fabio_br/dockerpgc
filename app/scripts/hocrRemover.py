from html.parser import HTMLParser
import os.path as os_path
import cv2

# Path hack.
from sys import path
import os.path as os_path
base_path = os_path.dirname(os_path.realpath(__file__))
root_path = os_path.dirname(base_path)

path.insert(0, root_path)

class Parser(HTMLParser):
    def __init__(self):
        self.boxes = []
        super().__init__()

    def handle_starttag(self, tag, attrs):
        is_word = False
        box = None
        for attr in attrs:
            if attr[0] =='class' and attr[1] =='ocrx_word':
                is_word = True
            elif attr[0] =='title' and attr[1].startswith('bbox'):
                box_str = attr[1].split(';')[0]
                box_str = list(map(int,box_str.split(' ')[1:]))
                box = ((box_str[0],box_str[1]),(box_str[2],box_str[3]))
        if not is_word:
            return
        self.boxes.append(box)

def get_hocr_boxes(file):
    parser = Parser()
    with open(file,'r',encoding='utf-8') as f:
        content = f.read()
        parser.feed(content)
    return parser.boxes

file_path = root_path+'/files/checkbox.hocr'
boxes = get_hocr_boxes(file_path)
img_file = root_path+'/imgs/checkbox.tiff'
img = cv2.imread(img_file)
#Make it gray
imgray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
#reducing noise
ret,thresh = cv2.threshold(imgray,127,255,0)
for box in boxes:
    cv2.rectangle(img,box[0],box[1],(255,255,255),-1)
cv2.imwrite('out.png',img)