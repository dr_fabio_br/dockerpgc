#! /usr/bin/env python3

# Path hack.
from sys import path
import os.path as os_path
base_path = os_path.dirname(os_path.realpath(__file__))
root_path = os_path.dirname(base_path)
path.insert(0, root_path)

import csv
import boxDetector.checkBoxDetector as cbd
from boxDetector.checkBoxDetector import flatten_line_segment
from boxDetector.checkBoxDetector import build_segments_angle_map,flatten_angles,flatten_line_segment


from boxDetector.lineSegment import LineSegment as Seg

files = ['real2','v','tick','x_on_corner','triangle']
# files = ['v']
for txt in files:
    points = []
    with open(root_path+'/output/'+txt+'.txt','r') as f:
        reader = csv.reader(f,delimiter=',')
        for row in reader:
            points.append([int(row[0]),int(row[1])])
    print("IS BOXY %s %s "%(txt,'YES' if cbd.is_boxy(points) else 'NO'))

# the_dic = {3.0: [Seg((11,3),(23,3))], 4.0:[ Seg((24,4),(39,4))], 5.0:[ Seg((40,5),(50,5))], 45.0: [Seg((12,45),(18,45))], 46.0: [Seg((23,46),(33,46)), Seg((19,46),(21,46))], 47.0: [Seg((47,47),(51,47)), Seg((37,47),(45,47)), Seg((34,47),(35,47))]}
# result = flatten_line_segment(the_dic)

# points = [(27,25),(58,25),(109,25),(119,19),(119,20),(118,21),(118,22),(117,23),(117,24),(116,25),(153,25),(154,26)]
# result = build_segments_angle_map(points)
# for angle in result:
#     print("\n\n")
#     print("Angle %f %s "%(((angle*180)/3.14),result[angle].lines))

# result2 = flatten_angles(result)
# print("FLAT!!!!\n")
# for angle in result2:
#     print("\n\n")
#     print("Angle %f %s "%(((angle*180)/3.14),result2[angle].lines))
# print("FLAT2!\n")
# print(flatten_line_segment(result2[0.0].lines))