from flask import current_app
from task_queues.queue import EmitterTaskRunner
from flask_json_rpc2.json_rpc import RPCHandler
from bson.objectid import ObjectId

def get_mq(name):
    config = current_app.config['runners'][name]
    runner = EmitterTaskRunner(config)
    runner.start()
    return runner

class Doc(RPCHandler):
    def __init__(self, get_model, elasticsearch):
        super().__init__()
        self._get_model = get_model
        self._es = elasticsearch

    def create(self, **params):
        dest = current_app.config['UPLOADED_FILES_DEST']
        model = self._get_model('Doc')
        ret = model.save_from_base64(params['file'], params['file_name'], dest)
        print(ret)
        if(ret.success):
            mq = get_mq('Doc')
            mq.send('file.raw',{'name':ret.result['name'],'_id':ret.result['_id']})
            mq.close();
            print("ENVIEI!")
            return ret.result
        else:
            raise ValidationError(ret.error)
    
    def list(self, **params):
        where = {'hocr':  {"$exists":True}}
        model = self._get_model('Doc')
        return model.list(where)

    def search(self, **params):
        if "searchString" not in params or not params["searchString"]:
            return self.list()
        body = {
            "query": {"regexp": {"content": ".*"+params["searchString"].lower()+".*"}},
            "highlight": {"fields" : {"content" : {}}}
        }
        res = self._es.search(index="tan_docs", doc_type="docs", body=body)
        print(res)
        if(res['hits']['total'] == 0):
            return None
        
        where = {'_id':{'$in':[]}}
        id_highlight_map = {}
        for hit in res['hits']['hits']:
            id_highlight_map[hit['_id']] = hit['highlight']['content']
            where['_id']['$in'].append(ObjectId(hit['_id']))
        where['hocr'] = {"$exists":True}
        model = self._get_model('Doc')
        data = model.collection.find(where)
        ret = []
        for d in data:
            d['highlights'] = id_highlight_map[str(d['_id'])]
            ret.append(d)
        return ret

    def readText(self,**params):
        dest = current_app.config['UPLOAD_ROOT']
        toRead = dest+'ocr/'+params['file']
        read_data = None
        with open(toRead, 'r') as f:
            read_data = f.read()
        return read_data