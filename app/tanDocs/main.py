from flask import g, current_app, Flask, Blueprint, jsonify, Response, send_from_directory, send_file
from flask.ext.pymongo import PyMongo
from flask import request
from flask.ext.cors import CORS
import importlib
from flask_json_rpc2.json_rpc import rpc_request
from tanDocs.models.CRUD import ValidationException
from task_queues.queue import EmitterTaskRunner
from flask_socketio import SocketIO
from tanDocs.controllers.Doc import Doc

from elasticsearch import Elasticsearch
es = Elasticsearch('elasticsearch')

## TODO: Refactor
socketio = SocketIO()

def get_model(name):
    return g._models.get(name, None)

def get_mq(name):
    config = current_app.config['runners'][name]
    runner = EmitterTaskRunner(config)
    runner.start()
    return runner

def initialize_models(mongo):
    model_map = {}
    models = ['Doc','Users']
    for name in models:
        module_name = 'tanDocs.models.%s'%name
        ModelClass = getattr(importlib.import_module(module_name), name)
        model =  ModelClass(mongo.db)
        model.build_indexes()
        model_map[name] = model
    return model_map

def initialize_runners(runners_config):
    runner_map = {}
    for name,config in runners_config.items():
        config = current_app.config['runners'][name]
        runner = EmitterTaskRunner(config)
        runner.start()
        runner_map[name] = runner
    return runner_map

def create_app(config= None):
    config_to_use = {
        'DEBUG':True,
        'MONGO_HOST' : 'mongo',
        'MONGO_DBNAME' : 'tanDocs',
        'UPLOADED_FILES_DEST' : '/file_outputs/raw/',
        'UPLOAD_ROOT' : '/file_outputs/',
        'runners':{
            'Doc':{
                'host':'mq',
                'exchange':'file_exchange'
            }
        }
    }
    if config:
        config_to_use.update(config)
    app = Flask(__name__)
    CORS(app)
    with app.app_context():
        app.config.update(config_to_use)
        app.register_blueprint(main)
        mongo = PyMongo(app)
        models = initialize_models(mongo)
        # runners = initialize_runners(app.config['runners'])
        @app.before_request
        def before_request():
            g.mongo = mongo
            g._models = models
            # g._runners = runners
    socketio.init_app(app)
    return app

main = Blueprint('main', __name__)
handler = Doc(get_model, es)
handler.register('/docs', main)

@main.route('/ping', methods=['POST'])
def ping():
    return 'pong'

@socketio.on('message')
def handle_message(message):
    print('received message: ' + message)

@socketio.on('connect')
def on_connect():
    print("SOMEONE Connected"+request.sid)

@socketio.on('disconnect')
def on_disconnect():
    print("SOMEONE LEFT"+request.sid)

@main.route('/img/<path:path>')
def send_js(path):
    upload_dir = current_app.config['UPLOAD_ROOT']
    print(upload_dir+path)
    return send_file(upload_dir+'raw/'+ path, mimetype='image/jpeg')

if __name__ == '__main__':
    app = create_app()    
    socketio.run(app,host='0.0.0.0',port=8000)

