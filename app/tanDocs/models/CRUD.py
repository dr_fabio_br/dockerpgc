from cerberus import Validator
import pymongo
from collections import namedtuple
insertion_result = namedtuple('insertion_result', 'success errors result')
insertion_result.__new__.__defaults__ = (None,) * len(insertion_result._fields)


update_result = namedtuple('update_result', 'success errors result matched modified')
update_result.__new__.__defaults__ = (None,) * len(update_result._fields)

delete_result = namedtuple('delete_result', 'success errors deleted')
delete_result.__new__.__defaults__ = (None,) * len(delete_result._fields)

index_format = namedtuple("index_format", "field direction unique")
index_format.__new__.__defaults__ = (None,) * len(index_format._fields)


class CRUD:
    __slots__ = ['validator', 'name','_db','_indexes']

    def __init__(self,name,schema,db,_indexes = None):
        self.name = name
        self._db = db
        self._indexes = _indexes
        self.validator = Validator(schema,transparent_schema_rules=True,allow_unknown=True)

    def build_indexes(self):
        if self._indexes is None:
            return
        for index in self._indexes:
            direction = getattr(pymongo, index.direction)
            print((index.field,direction))
            self.collection.create_index([(index.field,direction)],unique=index.unique)

    @property
    def collection(self):
        return  getattr(self._db,self.name)


    def _validate(self,data,update=False):
        return self.validator.validate(data,update=update)

    def _pre_create(self,data):
        return data

    def _post_create(self,data,insert_result):
        data['_id'] = str(insert_result.inserted_id)
        return data

    def create(self,data):
        if not self._validate(data):
            errors = self.validator.errors
            return insertion_result(False, errors)
        data = self._pre_create(data)
        ins = self.collection.insert_one(data)
        ret = self._post_create(data,ins)
        return insertion_result(True, None, ret)

    def update(self,where, data):
        if not self._validate(data,True):
            errors = self.validator.errors
            return update_result(False, errors)
        up = self.collection.update_one(where,{'$set':data})
        ret = data.copy()
        return update_result(True, None, ret,up.matched_count,up.modified_count)

    def upsert(self, where, data):
        if not self._validate(data,True):
            errors = self.validator.errors
            return update_result(False, errors)
        up = self.collection.update_one(where,{'$set':data},upsert = True)
        ret = data.copy()
        return update_result(True, None, ret,up.matched_count,up.modified_count)

    def delete(self,where):
        res = self.collection.delete_one(where)
        return delete_result(True, None, res.deleted_count)
    
    def list(self,where = None):
        return list(self.collection.find(where))

class ValidationException(Exception):
    def __init__(self,errors):
        self.errors = errors

    def __str__(self):
        return repr(self.errors)
#http://api.mongodb.org/python/current/api/pymongo/collection.html#pymongo.collection.Collection.update_one