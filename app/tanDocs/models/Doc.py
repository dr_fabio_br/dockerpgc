from tanDocs.models.CRUD import CRUD
from focUtils.nameGenerator import getRandomName
from collections import namedtuple
import base64
from io import BytesIO
from werkzeug import secure_filename
from werkzeug.datastructures import FileStorage
import os
import datetime
FileData = namedtuple("FileData", "name original_name extension")

class Doc(CRUD):
    def __init__(self,db):
        schema = {
            'name':{'required': True, 'type': 'string'},
            'original_name':{'required': True, 'type': 'string'},
            'extension':{'required': True, 'type': 'string'}
        }
        super().__init__('docs',schema,db)

    def save_file(self, file, dest='/tmp/'):
        random_name = getRandomName()
        out_file = dest+random_name
        file.save(out_file)
        filename = secure_filename(file.filename)
        extension = os.path.splitext(filename)[1][1:].strip().lower()
        return FileData(random_name,filename,extension)

    def file_from_base64(self,contents,name):
        decoded = base64.b64decode(contents)
        stream = BytesIO(decoded)
        return FileStorage(stream,name)

    def save_from_base64(self, file, filename ,dest='/tmp/'):
        f = self.file_from_base64(file, filename)
        file_data = self.save_file(f, dest)
        data = {
            'name':file_data.name,
            'original_name':file_data.original_name,
            'extension':file_data.extension,
            'date': datetime.datetime.utcnow()
        }
        return self.create(data)