from tanDocs.models.CRUD import CRUD, index_format
from collections import namedtuple

index_format = namedtuple("index_format", "field direction unique")
class Users(CRUD):
    def __init__(self,db):
        schema = {
            'name':{'required': True, 'type': 'string'},
            'email':{'required': True, 'type': 'string'},
            'password':{'required': True, 'type': 'string'}
        }
        _indexes = [index_format('email','ASCENDING',True)]
        super().__init__('users',schema,db,_indexes)