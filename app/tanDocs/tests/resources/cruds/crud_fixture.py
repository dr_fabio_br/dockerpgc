from bson.objectid import ObjectId

resources = {
    'mock':{
        'update':{
            'name':'Saved',
            'age':10,
            '_id':ObjectId()
        }
    }
}