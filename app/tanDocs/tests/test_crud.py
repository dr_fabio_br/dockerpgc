import unittest
from pymongo import MongoClient
from tanDocs.models.CRUD import CRUD
from tanDocs.tests.tools.fixtures import MongoFixture
from tanDocs.tests.resources.cruds.crud_fixture import resources

class CRUD_Test(unittest.TestCase):

    def setUp(self):
        self.fixture = MongoFixture('mongo')
        self._db =  self.fixture.connection['test_TanDocs']
        schema = {
            'name':{'required': True, 'type':'string','empty': False},
            'age':{'type':'number'}
        }
        crud = CRUD('mock', schema, self._db)
        self._crud = crud

    def test_failed_validation(self):
        res = self._crud._validate({'foo':'bar'})
        self.assertFalse(False)

    def test_sucessfull_validation(self):
        res = self._crud._validate({'name':'a name'})
        self.assertTrue(res)

    def test_insertion(self):
        data = {'name':'a name','age':10}
        res = self._crud.create(data)
        self.assertTrue(res.success)
        self.assertIsNone(res.errors)
        self.assertIsNotNone(res.result['_id'])
        self.assertEqual(res.result['name'],data['name'])
        self.assertEqual(res.result['age'],data['age'])


    def test_update(self):
        self.fixture.populate_collecton('test_TanDocs', resources) 
        _id = resources['mock']['update']['_id']
        res = self._crud.update({'_id':_id},{'age':100})
        self.assertTrue(res.success)
        self.assertIsNone(res.errors)
        self.assertEqual(res.matched,1)
        self.assertEqual(res.modified,1)
        item = self._db.mock.find_one({'_id':_id})
        self.assertEqual(item['age'],100)