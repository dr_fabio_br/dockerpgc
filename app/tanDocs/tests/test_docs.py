import requests
from pymongo import MongoClient
from bson.objectid import ObjectId
import os
import unittest
from flask import json
import os.path
import base64
from tanDocs.tests.tools.fixtures import MongoFixture
from tanDocs.tests.tools import json_rpc_request
from tanDocs.run import create_app
from unittest.mock import Mock, patch

class DocTest(unittest.TestCase):

    def setUp(self):
        config = {
            'TESTING':True,
            'MONGO_DBNAME':'test_TanDocs'
        }
        app = create_app(config)
        self._app = app
        self.client = app.test_client()

        self.fixture = MongoFixture('mongo')
        self._db =  self.fixture.connection['test_TanDocs']
        self._file_path = '/tmp/'

    @patch('tanDocs.run.EmitterTaskRunner')
    def test_insertion(self,mock_creator):
        data_base = self._app.config['MONGO_DBNAME']
        self.fixture.drop_collection(data_base, 'docs')
        mock_runner = Mock()
        mock_creator.return_value = mock_runner

        encoded = base64.b64encode(b'This is a test').decode()
        data =  {'file':encoded,'file_name':'test.pdf'}
        response = json_rpc_request(self.client,'/docs','create',data)
        file_data = response['result']
        self.assertTrue(os.path.isfile(self._file_path+file_data['name']) )
        resp = self._db.docs.find_one({'_id':ObjectId(file_data['_id'])})
        self.assertIsNotNone(resp)
        mock_creator.assert_called_with(self._app.config['runners']['Doc'])
        data = {'name':file_data['name'],'_id':file_data['_id']}
        mock_runner.send.assert_called_with('file.raw',data)