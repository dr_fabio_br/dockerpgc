from flask import Flask, json

def json_rpc_request(client,url, method, params=None, id=123):
    resp = client.post(
            url,
            data=json.dumps({
                'jsonrpc': '2.0',
                'id': id,
                'method': method,
                'params': params
            }),
            content_type='application/json'
    )
    response = json.loads(resp.get_data())
    return response