from pymongo import MongoClient

class MongoFixture:
    def __init__(self,*args,**kwargs):
        self.connection = MongoClient(*args,**kwargs)

    def drop_collection(self, data_base, collection):
        db = self.connection[data_base]
        db[collection].drop()

    def populate_collecton(self, data_base, data, drop_before=True):
        db = self.connection[data_base]
        for collection,collection_data in data.items():
            if drop_before:
                db.drop_collection(collection)
            if isinstance(collection_data, dict):
                collection_data = list(collection_data.values())
            db[collection].insert_many(collection_data)
        return True
