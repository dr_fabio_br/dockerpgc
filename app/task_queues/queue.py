import pika
import pickle
import logging
import abc
import signal
from collections import namedtuple


def run_task_runner(runner):
    try:
        runner.start()

        def signal_handler(signal, frame):
            runner.close()
        signal.signal(signal.SIGINT, signal_handler)

    except KeyboardInterrupt:
        runner.close()

class EmitterTaskRunner:
    def __init__(self, config):
        self._config = config

    def _start_emitter(self):
        host = self._config['host']
        exchange = self._config['exchange']
        self._emitter = QEmitter(host, exchange)

    def _set_up(self):
        pass

    def start(self):
        self._set_up()
        self._start_emitter()

    def close(self):
        self._emitter.close()

    def send(self, key, data):
        self._emitter.send(key, data)

class ConsumerTaskRunner:

    def __init__(self, config):
        self._config = config
        self.to_run = self.task

    def _set_up(self):
        pass

    def start(self):
        self._set_up()
        self._start_consumer()

    def close(self):
        self._consumer.close()

    @abc.abstractmethod
    def task(self, key, data):
        pass

    def _start_consumer(self):
        host = self._config['host']
        exchange = self._config['exchange']
        consumer = QConsumer(host, exchange)
        self._consumer = consumer
        topic_name = self._config['topic']
        consumer.bind(topic_name, self.to_run)


class ConsumerEmitterTaskRunner(ConsumerTaskRunner,EmitterTaskRunner):
    """Consumes a queue and emit a task with the result
    """
    __metaclass__ = abc.ABCMeta

    def __init__(self, config):
        ConsumerTaskRunner.__init__(self,config)
        EmitterTaskRunner.__init__(self,config)
        self.to_run = self._task

    def start(self):
        EmitterTaskRunner.start(self)
        ConsumerTaskRunner.start(self)

    def close(self):
        EmitterTaskRunner.close(self)
        ConsumerTaskRunner.close(self)

    @abc.abstractmethod
    def emitter_task(self, key, data, result):
        pass
    
    def _task(self, key,data):
        result = self.task( key, data)
        if result is False or getattr(result,'success') is False:
            return False
        emitter_result = self.emitter_task( key, data, result)
        return emitter_result
    

class QConsumer:

    def __init__(self, host, exchange, queue='', durable=False):
        """
        Args:
            host(string): rabbit host
            exchange(string): name of the exchange
            queue(string): name of queue
            durable(boolean): 
        """
        params = pika.ConnectionParameters(host)
        connection = pika.BlockingConnection(params)
        channel = connection.channel()
        channel.basic_qos(prefetch_count=1)
        channel.exchange_declare(exchange=exchange,
                                 type='topic')
        result = channel.queue_declare(queue=queue, durable=durable)
        self._queue = result.method.queue
        self._exchange = exchange
        self._ch = channel
        self._conn = connection

    def close(self):
        self._conn.close()

    def bind(self, routing_key, task):
        channel = self._ch
        channel.queue_bind(exchange=self._exchange,
                           queue=self._queue,
                           routing_key=routing_key)

        def callback(ch, method, properties, body):
            key = method.routing_key
            data = pickle.loads(body)
            ret_task = task(key, data)
            make_ack = False
            try:
                make_ack = getattr(ret_task,'success')
            except AttributeError:
                make_ack = ret_task
            if make_ack:
                ch.basic_ack(delivery_tag=method.delivery_tag)

        channel.basic_consume(callback,
                              queue=self._queue)
        channel.start_consuming()


class QEmitter:

    def __init__(self, host, exchange, type='topic'):
        self.host = host
        self.exchange = exchange
        self.type = type
        self._connect()

    
    def _connect(self):
        params = pika.ConnectionParameters(self.host)
        self._conn = pika.BlockingConnection(params)
        self._ch = self._conn.channel()
        self._ch.exchange_declare(exchange=self.exchange,
                                  type=self.type)

    def _reconnect(self):
        self._connect()

    def send(self, key, data):
        try:
            self._ch.basic_publish(exchange=self.exchange,
                                   routing_key=key,
                                   body=pickle.dumps(data))
        except pika.exceptions.ConnectionClosed:
            self._reconnect()
            self._ch.basic_publish(exchange=self.exchange,
                                   routing_key=key,
                                   body=pickle.dumps(data))

    def close(self):
        self._conn.close()
