import unittest
import pickle
from task_queues.queue import (
    QConsumer,
    ConsumerTaskRunner
)
from unittest.mock import Mock, patch
from collections import namedtuple

config = {
    'host':'mq',
    'exchange':'test',
    'topic':'topic'
}

class ConsumerRunner(ConsumerTaskRunner):
    def task(self, key, data):
        pass

class MockConnecton:
    def __init__(self):
        self._ch = Mock()
    
    def channel(self):
        return self._ch

    def close(self):
        pass

class ConsumerTest(unittest.TestCase):
    @patch('task_queues.queue.pika.BlockingConnection')
    @patch('task_queues.queue.pika.ConnectionParameters')
    def test_q_consummer(self,mock_params,mock_block):
        params =  {"FOO":"BAR"}
        mock_params.return_value = params
        conn = MockConnecton()
        mock_conn = Mock(wraps=conn)
        mock_block.return_value = mock_conn
        mock_declare = Mock()
        mock_queue = Mock()
        mock_declare.method.queue = mock_queue
        conn._ch.queue_declare.return_value = mock_declare
        
        consumer = QConsumer(config['host'],config['exchange'])
       
        mock_params.assert_called_with(config['host'])
        mock_block.assert_called_with(params)
        mock_conn.channel.assert_called_with()
        conn._ch.exchange_declare.assert_called_with(exchange=config['exchange'],type='topic')
        conn._ch.queue_declare.assert_called_with(queue='',durable=False)

        mock_task = Mock()
        routing = 'test.test'
        consumer.bind(routing, mock_task)

        conn._ch.queue_bind.assert_called_with(exchange=config['exchange'],
                           queue=mock_queue,
                           routing_key=routing)

        conn._ch.start_consuming()
        callback = conn._ch.basic_consume.call_args[0][0]

        mock_body = {'foo':'bar'}
        picked_body = pickle.dumps(mock_body)
        
        method = namedtuple('method','routing_key delivery_tag')
        call_method = method('baz','xpto')
        #Testing success callback
        mock_task.return_value = True
        callback(conn._ch,call_method,None,picked_body)
        mock_task.assert_called_with(mock_body,call_method.routing_key)
        conn._ch.basic_ack.assert_called_with(delivery_tag=call_method.delivery_tag)

        #Testing failed callback
        conn._ch.basic_ack.reset_mock()
        mock_task.return_value = False
        callback(conn._ch,call_method,None,picked_body)
        conn._ch.basic_ack.assert_not_called()

    @patch('task_queues.queue.QConsumer')
    def test_consumer_runner(self,mock_consumer):
        consumer_obj = Mock()
        mock_consumer.return_value = consumer_obj
        runner = ConsumerRunner(config)
        runner._set_up = Mock()
        runner.start()
        runner._set_up.assert_called_with()
        mock_consumer.assert_called_with(config['host'], config['exchange'])
        consumer_obj.bind.assert_called_with(config['topic'],runner.task)
