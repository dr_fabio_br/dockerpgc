import unittest
import pickle
from task_queues.queue import (
    QEmitter,
    ConsumerEmitterTaskRunner
)
from unittest.mock import Mock, patch


config = {
    'host':'mq',
    'exchange':'test',
    'topic':'topic'
}

class Runner(ConsumerEmitterTaskRunner):
    def task(self, key, data):
        pass

    def emitter_task(self, key, data, result):
        pass

class EmiterTest(unittest.TestCase):

    @patch('task_queues.queue.QConsumer')
    @patch('task_queues.queue.QEmitter')
    def test_emiter_task_runner(self, mock_emitter,mock_consumer):
        consumer = Mock()
        emitter = Mock()
        mock_emitter.return_value = emitter
        mock_consumer.return_value = consumer
        
        runner = Runner(config)
        runner.task = Mock()
        mock_result = {"foo":"bar"}
        runner.task.return_value = mock_result
        runner.emitter_task = Mock()
        runner.start()

        #Check that the task bound to the consumer is _task and not task 
        consumer.bind.assert_called_with(config['topic'],runner._task)
        #Check that when we run _task simulating bind we pass the result to emiter task
        mock_key = 'foo.key.bar'
        mock_data = {'baz':'zip'}
        
        runner._task(mock_key, mock_data)
        runner.task.assert_called_with(mock_key, mock_data)
        runner.emitter_task.assert_called_with(mock_key, mock_data, mock_result)