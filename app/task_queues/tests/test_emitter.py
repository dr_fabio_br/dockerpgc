import unittest
import pickle
from task_queues.queue import (
    QEmitter,
    EmitterTaskRunner
)
from unittest.mock import Mock, patch

class MockConnecton:
    def __init__(self):
        self._ch = Mock()
    
    def channel(self):
        return self._ch

    def close(self):
        pass


config = {
    'host':'mq',
    'exchange':'test'
}

class EmiterTest(unittest.TestCase):

    @patch('task_queues.queue.pika.BlockingConnection')
    @patch('task_queues.queue.pika.ConnectionParameters')
    def test_q_emiter(self,mock_params,mock_block):
        params =  {"FOO":"BAR"}
        mock_params.return_value = params
        conn = MockConnecton()
        mock_conn = Mock(wraps=conn)
        mock_block.return_value = mock_conn
        
        emitter = QEmitter(config['host'],config['exchange'])
       
        mock_params.assert_called_with(config['host'])
        mock_block.assert_called_with(params)
        mock_conn.channel.assert_called_with()
        conn._ch.exchange_declare.assert_called_with(exchange=config['exchange'],type='topic')

        key = 'test.test.test'
        data = 'foo'
        emitter.send(key,data)

        conn._ch.basic_publish.assert_called_with(
            exchange=config['exchange'],
            routing_key=key,
            body=pickle.dumps(data)
        )

        emitter.close()
        mock_conn.close.assert_called_with()

    @patch('task_queues.queue.QEmitter')
    def test_emiter_task_runner(self, mock_emitter):
        emitter_obj = Mock()
        mock_emitter.return_value = emitter_obj
        runner = EmitterTaskRunner(config)
        runner._set_up = Mock()
        runner.start()
        runner._set_up.assert_called_with()
        mock_emitter.assert_called_with(config['host'],config['exchange'])
        key = 'test.test.test'
        data = 'foo'
        runner.send(key,data)
        emitter_obj.send.assert_called_with(key,data)
        runner.close()
        emitter_obj.close.assert_called_with()