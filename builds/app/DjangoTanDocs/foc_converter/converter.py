from utils.nameGenerator import RandomName
from subprocess import call
from collections import namedtuple
import os

baseDir = os.path.dirname(__file__)

class Converter:
	@staticmethod
	def convert_to_tiff(file_name,upload_dir):
		Ret = namedtuple("Ret","success tiff")
		tiffName=RandomName.getRandomName()+".tiff"
		retcode = call(["convert", "-density","300","-depth","8",file_name,"-background","white","-alpha","remove",upload_dir+tiffName])
		if retcode < 0:
			return Ret(False,None)
		return Ret(True,tiffName)

	@staticmethod
	def convert_to_hocr(tiff_file,upload_dir):
		Ret = namedtuple("Ret","success hocr")
		hocrFileName=RandomName.getRandomName()
		#the config data is where it says to convert HOCR
		retcode=call(["tesseract",tiff_file,upload_dir+hocrFileName,"-l","por","+configData/hocr.txt"],cwd=baseDir+"/cmd")
		if retcode < 0:
			return Ret(False,None)
		return Ret(True,hocrFileName+".hocr")

	@staticmethod
	def convert_to_ocr(tiff_file,upload_dir):
		Ret = namedtuple("Ret","success ocr")
		ocrFileName=RandomName.getRandomName()
		#the config data is where it says to convert HOCR
		retcode=call(["tesseract",tiff_file,upload_dir+ocrFileName,"-l","por"],cwd=baseDir+"/cmd")
		if retcode < 0:
			return Ret(False,None)
		return Ret(True,ocrFileName+".txt")
