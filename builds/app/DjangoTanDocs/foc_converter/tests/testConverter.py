import sys
import os
from foc_files.tests.utils import cleanFolder
from django.test import TestCase
from ..converter import Converter

baseDir = os.path.dirname(__file__)

class TestConverter(TestCase):
	def setUp(self):
		self.uploadDir = os.path.join(baseDir, 'uploadDir/')
		self.resourceDir = os.path.join(baseDir, 'resources/')
		self.assertTrue(cleanFolder(self.uploadDir))

	def test_convert_file(self):
		fileName = self.resourceDir+'simpleEscaned.jpg'
		data = Converter.convert_to_tiff(fileName,self.uploadDir)
		self.assertTrue(data.success)
		self.assertIsNotNone(data.tiff)
		self.assertTrue(os.path.isfile(self.uploadDir+data.tiff))

	def test_convert_hocr(self):
		fileName = self.resourceDir+'converted.tiff'
		data = Converter.convert_to_hocr(fileName,self.uploadDir)
		self.assertTrue(data.success)
		self.assertIsNotNone(data.hocr)
		self.assertTrue(os.path.isfile(self.uploadDir+data.hocr))

	def test_convert_ocr(self):
		fileName = self.resourceDir+'converted.tiff'
		data = Converter.convert_to_ocr(fileName,self.uploadDir)
		self.assertTrue(data.success)
		self.assertIsNotNone(data.ocr)
		self.assertTrue(os.path.isfile(self.uploadDir+data.ocr))
