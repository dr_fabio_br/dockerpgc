from elasticsearch import Elasticsearch
from collections import namedtuple

class ElasticSearch:
	def __init__(self):
		self.es = Elasticsearch()

	def save_document(self,document,index,doc_type,id=None):
		Ret = namedtuple("Ret","success id")
		res = self.es.index(index=index, doc_type=doc_type, body=document)
		if not res['created']:
			return Ret(False,None)
		return Ret(True,res['_id'])
