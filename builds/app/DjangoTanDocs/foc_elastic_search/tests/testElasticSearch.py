from django.test import TestCase
from ..elasticSearch import ElasticSearch as ESModel
from elasticsearch import Elasticsearch

class TestElasticSearch(TestCase):
	def setUp(self):
		self.model = ESModel()
		self.es = Elasticsearch()
		res = self.es.indices.delete(index='test_tan_docs', ignore=[400, 404])
		self.assertTrue(res['acknowledged'])

	def test_doc_insertion(self):

		doc={
			"contents":"foo"
		}
		res = self.model.save_document(doc,'test_tan_docs','save_test')
		self.assertTrue(res.success)
		self.assertIsNotNone(res.id)
