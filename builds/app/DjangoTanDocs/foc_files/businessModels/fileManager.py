import os
from utils.nameGenerator import RandomName
from foc_schema.models import Document

class FileNotAllowed(Exception):
    pass
class FileManager:
    def __init__(self,allowed_extensions=None,save_dir=None):
        self._extensions = allowed_extensions
        self._save_dir = save_dir

    def isExtensionAllowed(self,extension):
        if(extension not in self._extensions):
                return False
        return True

    def isFileAllowed(self,file):
        extension=os.path.splitext(file.name)[1][1:]
        return self.isExtensionAllowed(extension)

    def saveFile(self,file):
        if(not self.isFileAllowed(file)):
            raise FileNotAllowed("the file is not allowed")
        randomName= RandomName.getRandomName(file.name)
        destFile=self._save_dir+randomName
        with open(destFile, 'wb+') as destination:
            for chunk in file.chunks():
                destination.write(chunk)
        return randomName

    def saveFileToDatabase(self,originalName,savedName,extension):
        return Document.objects.create(
            originalName=originalName,
            file=savedName,
            originalExtension=extension
        )