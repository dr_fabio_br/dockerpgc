from foc_files.businessModels.fileManager import FileManager
from django.core.files.uploadedfile import SimpleUploadedFile
import sys
import os
from .utils import cleanFolder
from django.test import TestCase

baseDir = os.path.dirname(__file__)

class TestFileManager(TestCase):
    fixtures = []
    def setUp(self):
        self.uploadDir = os.path.join(baseDir, 'uploadDir/')
        self.assertTrue(cleanFolder(self.uploadDir))
        self.manager = FileManager(['gif','jpeg','jpg'],self.uploadDir)
        dir = os.path.dirname(__file__)
        filename = os.path.join(dir, 'resources/simpleEscaned.jpg')
        f = open(filename, 'rb')
        self.validImage = SimpleUploadedFile('simpleEscaned.jpg', f.read(), content_type='image/jpg')
        self.invalidTxt = SimpleUploadedFile('txt.txt', 'Foo'.encode('utf-8'), content_type='text/plan')

    def test_extensions_allowed(self):
        self.assertTrue(self.manager.isExtensionAllowed('jpg'))
        self.assertFalse(self.manager.isExtensionAllowed('svg'))

    def test_when_file_allowed(self):
        self.assertTrue(self.manager.isFileAllowed(self.validImage))

    def test_when_file_disallowed(self):
        self.assertFalse(self.manager.isFileAllowed(self.invalidTxt))

    def test_save_file_on_db(self):
        originalName = "originalName"
        fileName = "savedName"
        originalExtension = "ext"
        doc = self.manager.saveFileToDatabase(originalName,fileName,originalExtension)
        self.assertEqual(originalName,doc.originalName)
        self.assertEqual(originalExtension,doc.originalExtension)
        self.assertEqual(fileName,doc.file)

    def test_file_save(self):
        self.assertIsInstance(self.manager.saveFile(self.validImage),basestring)
