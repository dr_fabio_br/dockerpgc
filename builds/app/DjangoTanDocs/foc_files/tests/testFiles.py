import sys
import os, shutil
from django.test import TestCase
from django.test import Client

from .utils import cleanFolder

baseDir = os.path.dirname(__file__)
class TestFiles(TestCase):

    def setUp(self):
        self.uploadDir = os.path.join(baseDir, 'uploadDir/')
        self.assertTrue(cleanFolder(self.uploadDir))

    def test_file_upload(self):
        c = Client()
        filename = os.path.join(baseDir, 'resources/simpleEscaned.jpg')
        with open(filename,'rb') as fp:
            response = c.post('/files/upload', {'file': fp})
        self.assertEqual(response.status_code, 200)
        print(response.content)
        print(response.context)

    def tearDown(self):
        print("tearing down")