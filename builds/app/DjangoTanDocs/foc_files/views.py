from django.shortcuts import render
from django.views import generic
import json
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.utils.decorators import method_decorator
from django.conf import settings
from subprocess import call

class ConvertView(generic.TemplateView):
	@method_decorator(csrf_exempt)
	def post(self, request, *args, **kwargs):
		file = request.FILES['file']
		return JsonResponse({'success':True})
