# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created_on', models.DateTimeField(auto_now_add=True, null=True)),
                ('file', models.CharField(max_length=255)),
                ('originalName', models.CharField(max_length=255)),
                ('originalExtension', models.CharField(max_length=4)),
                ('tiffFile', models.CharField(max_length=255)),
                ('ocrText', models.CharField(max_length=255)),
                ('hocrFile', models.CharField(max_length=255)),
                ('ocrFile', models.CharField(max_length=255)),
                ('notesFile', models.CharField(max_length=255)),
                ('isOnElastic', models.BooleanField()),
            ],
            options={
                'db_table': 'documents',
            },
        ),
    ]
