from django.db import models
class Document(models.Model):
	collectionName='documents'
	created_on = models.DateTimeField(auto_now_add=True, null=True)
	file = models.CharField(max_length=255)
	originalName = models.CharField(max_length=255)
	originalExtension = models.CharField(max_length=4)
	tiffFile = models.CharField(max_length=255)
	ocrText  = models.CharField(max_length=255)
	hocrFile = models.CharField(max_length=255)
	ocrFile  = models.CharField(max_length=255)
	notesFile  = models.CharField(max_length=255)
	isOnElastic = models.BooleanField(default=False)
	class Meta:
		db_table='documents'