    def is_boxy(points, angle_epsilon=8, parallel_epsilon=0.5, min_size=5,max_size=30, gap_epsilon=10):
        """Verifica se um conjunto de pontos parece uma caixa
        Args:
            points: Lista de contornos
            angle_epsilon: quanto os segmentos de linha podem diferir
            parallel_epsilon: quanto um conjunto de linhas paralelas podem se afastar
            min_size: tamanho minimo da caixa
            max_size: Tamanho maximo da caixa
            gap_epsilon: Quanto uma linha pode ser descontinuada
       """